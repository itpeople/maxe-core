.. Maxe core documentation master file, created by
   sphinx-quickstart on Fri Aug 30 18:26:19 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Maxe core's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Build steps:
1) checkout maxe-core, install it.
2) checkout project
3) create logs dir
4) python manage.py syncdb
5) python manage.py migrate