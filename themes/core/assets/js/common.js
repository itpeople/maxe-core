
jQuery(document).ajaxSend(function(event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    function sameOrigin(url) {
        // url "could" be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }
    function safeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
});

var getSubEl = function(html, selector) {
    var rscript = /<script(.|\s)*?\/script>/gi
    var result = jQuery("<div />")
        // inject the contents of the document in, removing the scripts
        // to avoid any 'Permission Denied' errors in IE
        .append(html.replace(rscript, ""))

        // Locate the specified elements
        .find(selector);
    return result
}

var loadAjaxData = function(htmlResponse) {

    var loadedNotifyTriggers = {};
    $('.notify-trigger').each(function(){
        var id = $(this).data('notify-id');
        if (!id) {
            console.log('One of notify triggers has no id!')
        }
        loadedNotifyTriggers[id] = true;
    });

    getSubEl(htmlResponse, '.ajax-replace').each(function(){
        var el = $(this);
        var rclass = el.data('replace-class');
        if (rclass) {
            $('.' + rclass).html(el.html())
        }
    });

    var needReload = false;

    getSubEl(htmlResponse, '.notify-trigger').each(function(){
        var el = $(this);
        var id = el.data('notify-id');
        if (!id) {
            console.log('One of notify triggers has no id!')
        } else {
            if (!loadedNotifyTriggers[id]) {
                var rclass = el.data('notify-reload-on')
                if (rclass) {
                    if($('.' + rclass)) {
                        needReload = true;
                    }
                }
            }
        }
    });

    if (needReload) {
        $.ajax({
          url: window.location,
          type: "GET"
        }).success(function(data) {
            loadAjaxData(data)
        });
    }
};
