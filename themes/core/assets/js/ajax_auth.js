

var ajax_prepare_form = function(selector) {

    var on_submit = function(){

        var data = $(selector + ' form').serialize();

        $(selector + ' .form-error').html('');

        $(selector + ' form .error-message').hide();

        $.ajax({
            url: $(selector + ' form').attr('action'),
            type: 'post',
            data: data,
            success: function(data){

                if (!data.ok) {
                    for (var field in data.errors) {

                        var error = data.errors[field]
                        error = typeof error == 'string' ? error : error.join(', ')

                        if (field == '__all__') {
                            $(selector + ' .form-error').html(error);
                        } else {
                            var el = $(selector + " input[name=" + field + "]");

                            el.siblings('.error-message').show().html(error);
                            el.attr('rel', 'tooltip');
                            el.attr('data-original-title', error);
                            el.tooltip({
                                selector: el,
                                placement: 'right'
                            })

                            el.addClass('error');
                        }

                    }
                } else {
                    location.reload();
                }
            }
        });

        return false;
    };
    $(selector + " .submit-btn").click(on_submit);
    $(selector + " form").submit(on_submit);
}

$(function() {

    $("#account-dialog input").each(function(){
        $(this).data('pl', $(this).attr('placeholder'));
    });
    $("#account-dialog input").focus(function(){
        $(this).removeClass('error');
        $(this).attr('placeholder', $(this).data('pl'));
    });

    ajax_prepare_form('#login');
    ajax_prepare_form('#register');
    ajax_prepare_form('#forgot');


});
