

flyToPointEffect = function(flyingEl, gotoX, gotoY, onComplete) {

    var newImageWidth = 60;
    var newImageHeight = 60;

    flyingEl.clone()
        .css({
            'position' : 'absolute',
            left: flyingEl.offset().left,
            top: flyingEl.offset().top,
            width: flyingEl.height(),
            width: flyingEl.width(),
            'z-index': 1000
        })
        .insertAfter(flyingEl)
        .animate({opacity: 0.8}, 100 )
        .animate({opacity: 0.1, left: gotoX, top: gotoY, width: newImageWidth, height: newImageHeight}, 800, function() {
            $(this).remove();

            onComplete();
        });
};

kickBasket = function(basket, onComplete) {
    basket.addClass('kicked2');
    setTimeout(function(){
        basket.removeClass('kicked2');
        basket.addClass('kicked');
        setTimeout(function(){
            basket.removeClass('kicked');
            onComplete();
        }, 500);
    }, 500);
}


var load_cart_data = function(data) {
    loadAjaxData(data)
}

var remove_cart_product = function(id) {

    $.ajax({
        url: '/cart/remove/' + id + '/0',
        success: function(data){
            load_cart_data(data);

            $("#page_product_remove_" + id).hide();
            $("#page_product_add_" + id).show();
        }
    });

    return false;
}

var cart_render_compare = function() {

    $.ajax({
        url: '/compare/render',
        success: function(data){
            loadAjaxData(data);
        }
    });

    return false;
}

var cart_add_to_compare = function(id) {

    $.ajax({
        url: '/compare/add/' + id,
        success: function(data){
            loadAjaxData(data);
        }
    });

    return false;
}

var cart_incr_product_count = function(id) {

    $.ajax({
        url: '/cart/add/' + id + '/1',
        success: function(data){
            load_cart_data(data);
        }
    });

    return false;
}
var cart_set_product_count = function(id, qty) {

    $.ajax({
        url: '/cart/qty/' + id + '/' + qty,
        success: function(data){
            load_cart_data(data);
        }
    });

    return false;
}
var cart_set_product_delivery = function(id, method) {

    $.ajax({
        url: '/cart/delivery/' + id + '/' + method,
        success: function(data){
            load_cart_data(data);
        }
    });

    return false;
}

var cart_decr_product_count = function(id) {

    $.ajax({
        url: '/cart/remove/' + id + '/1',
        success: function(data){
            load_cart_data(data);
        }
    });

    return false;
}

var clear_cart = function() {

    $.ajax({
        url: '/cart/clear',
        success: function(data){
            load_cart_data(data);
        }
    });

    return false;
}

var autocheckout_if_needed = function(btn) {
    if (btn.hasClass('product-add-autocheckout')) {
        location.href = global_checkout_url;
    }
}

$(function() {

    $(window).scroll(function(){
        if($(window).scrollTop() > 50) {
            $("#shoping-cart").addClass('scrolled');
        } else {
            $("#shoping-cart").removeClass('scrolled');
        }
    });


    $('body').on('click', '.add-to-compare', function() {
        $.ajax({
            url: $(this).attr("href"),
            success: function(data){
                loadAjaxData(data);
            }
        });
        return false;
    });


    $('body').on('click', '.add-to-compare-trigger', function() {
        $.ajax({
            url: $(this).attr("href"),
            success: function(data){
                loadAjaxData(data);
            }
        });
        return false;
    });


    $('body').on('click', '.clear-compare-trigger', function() {
        $.ajax({
            url: $(this).attr("href"),
            success: function(data){
                loadAjaxData(data);
            }
        });
        return false;
    });




    $(".product .product-add-trigger").each(function(key, btn){
        var btn = $(this);
        btn.click(function(){
            var basket = $("#cart")

            var product = btn.parents(".product");

//            btn.button('loading'); // for loading

            var cart_data = null;
            var in_basket = false;

            $.ajax({
                url: btn.attr("data-href"),
                success: function(data){
                    if (in_basket) {
                        load_cart_data(data);
                        autocheckout_if_needed(btn);
                    } else {
                        cart_data = data;
                    }
                }
            });

//            flyToPointEffect(product, basket.offset().left -350, $(window).scrollTop() + 10, function(){
                kickBasket(basket, function(){
//                    console.log("Animation completed!");
                });
                in_basket = true;
                if (cart_data) {
                    load_cart_data(cart_data);
                    autocheckout_if_needed(btn);
                } else {
                    $('#cart').click();
                }
//                btn.button('reset'); // for loading

//
//            });



            return false;

        });
    });
});
