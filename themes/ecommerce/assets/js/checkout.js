
function render_place_name(texts, place) {
    var text = '';

    text += place.name
    text += ' - ' + texts.address + ', ' + place.city + ', ' + place.address + '. '  +  place.opened
    if (place.description) {
        text += ' (' + place.description  + ')'
    }
    return text;
}

function render_place(texts, place) {
    var html = '';

    html += '<table>'
    html += '<tr><td width="100">' + texts.address + ' </td><td>' + place.city + ', ' + place.address + '</td></tr>'
    if (place.description) {
        html += '<tr><td></td><td>' + place.description + '</td></tr>'
    }
    html += '<tr><td> </td><td>' + place.opened + '</td></tr>'
    html += '<table>'

    if (!place.active) {
        html += '<span style="color: red;">' + texts.not_working + '</span>';
        html += '<p>' + texts.not_working_reaon + ' ' + place.inactive_reason + '</p>';
    }
    return html;
}
function select_place(texts, place) {
    var html = render_place(texts, place);
    $("#smartpost-selected").html(html);
}
function load_smartpost_selector(texts, selected_place, change_listener) {
    var places = {}
    var groups = {};
    var groupList = [];

    for (var i in places_info) {
        var place = places_info[i];
        if (!groups[place.group_id]) {
            groups[place.group_id] = {
                id:place.group_id,
                name:place.group_name,
                sort:place.group_sort,
                items:[
                    place
                ]
            }
            groupList.push({id:place.group_id, sort:place.group_sort});
        } else {
            groups[place.group_id].items.push(place);
        }
        places[place.place_id] = place

    }

    groupList.sort(
        function (a, b) {
            if (a.sort < b.sort)
                return -1;
            if (a.sort > b.sort)
                return 1;
            return 0;
        }).reverse();

    var html = '<select id="smartpost-selector"><option value="-">-- ' + texts.select_empty + ' --</option>'

    for (var g in groupList) {
        var group = groups[groupList[g].id];
        html += '<optgroup label="' + group.name + '">';

        for (var p in group.items) {
            var place = group.items[p];
            html += '<option value="' + place.place_id + '"' + (place.place_id == selected_place ? ' selected="selected"' : '') + '>' + place.name + '</option>';
        }
        html += '</optgroup>';
    }
    html += '</select>';

    $("#smartpost-places").html(html);

    var on_change = function () {
        var place = places[$(this).val()];

        change_listener(place)

        select_place(texts, place);
    };
    $("#smartpost-selector").change(on_change);

    if (selected_place) {
        select_place(texts, places[selected_place]);
    }
}