__author__ = 'Alex'

import pkg_resources


def cms(config):
    return {
        "icon": "",
        "title": "Basic CMS features",
        "description": "",

        "require": [],

        "config": {
            "applications": [
                'maxe.core.common',
                'maxe.core.content',
            ],
            "contexts": [
                "maxe.core.common.context.common_context"
            ],
            "feature_urls": [],
        }
    }


def profile(config):
    return {
        "icon": "",
        "title": "Core prfile",
        "description": "",

        "require": [],

        "config": {
            "applications": [
                'maxe.core.profile',
            ],
            "contexts": [],
            "feature_urls": [],
        }
    }


def debug(config):
    return {
        "icon": "",
        "title": "Debug tools",
        "description": "",
        "admin_only": True,

        "require": [],

        "config": {
            "applications": [
                'debug_toolbar',
            ],
            "contexts": [
            ],
            "feature_urls": [],
        }
    }


def social_auth(config):

    return {
        "icon": "",
        "title": "Social auth",
        "description": "",

        "require": [],

        "config": {
            "applications": [
                'social_auth',
                'maxe.core.facebook_login',
            ],
            "auth_backends": [
                'social_auth.backends.facebook.FacebookBackend',
                'social_auth.backends.twitter.TwitterBackend',
            ],
            "contexts": [
#                'social_auth.context_processors.social_auth_by_name_backends',
#                'social_auth.context_processors.social_auth_backends', # don't put with by_type
#                'social_auth.context_processors.social_auth_by_type_backends',
                'social_auth.context_processors.social_auth_login_redirect',
            ],
            "feature_urls": [
                'social_auth.urls'
            ],
        }
    }


#
# FEATURES_MAP = {
#
#
#     "debug": ,
#
#     "awesomebit": {
#         "icon": "",
#         "title": "Awesomebit website",
#         "description": "",
#         "admin_only": True,
#
#         "require": ["cms",],
#
#         "config": {
#             "applications": [
#                 "maxe.fabric.tasks",
#                 "maxe.fabric.profile",
#                 "maxe.fabric.deployments",
#             ],
#             "contexts": [],
#             "feature_urls": [
#                 "maxe.fabric.profile.urls",
#                 "maxe.fabric.deployments.urls"
#             ],
#         }
#     },
#
#     "web-shop": ,
#
#

    # "shop-extra":
# }


