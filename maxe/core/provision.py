from pywizard.resources.package import package
from pywizard.utils.process import require_sudo

require_sudo()

package('python-imaging  python-dev libjpeg8 libjpeg8-dev libfreetype6 libfreetype6-dev'.split(' '))

package('libmemcached-dev')
package('libgeoip1')