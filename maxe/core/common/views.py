

def clear_cache(request):
    from django.core.cache import cache
    from django.core.urlresolvers import reverse
    from django.http import HttpResponseRedirect
    from django.contrib import messages

    try:
        cache._cache.flush_all()
    except AttributeError:
        pass

    try:
        cache._cache.clear()
    except AttributeError:
        pass
    try:
        cache._expire_info.clear()
    except AttributeError:
        pass
    messages.info(request, "Cache Cleared")
    return HttpResponseRedirect(reverse("admin:index"))
