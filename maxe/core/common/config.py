import json
from django.utils.functional import lazy
import os
import sys
import pkg_resources
import yaml
from maxe.core.common.theming import load_theme


def merge_feature(config, all_features, feature_name):

    if feature_name not in all_features:
        raise Exception('Feature %s is not installed' % feature_name)

    try:
        feature_plugin = all_features[feature_name].load()
    except ImportError as e:
        raise Exception('Error loading feature %s: %s' % (feature_name, e.message))

    feature = feature_plugin(config)

    config['loaded_features'].append(feature_name)

    for requirement in feature['require']:
        if requirement in config['loaded_features']:
            continue
        merge_feature(config, all_features, requirement)

    for key, val in feature['config'].items():
        if not config.has_key(key):
            config[key] = val
        else:
            config[key] += val

def load_config(filename, app_path):
    try:
        with open(filename, 'r') as f:
            config = yaml.load(f)

        config['loaded_features'] = []

        all_features = {}
        for entry_point in pkg_resources.iter_entry_points('maxe.features'):
            all_features[entry_point.name] = entry_point

        # FEATURES
        for feature_name in config["features"]:
            merge_feature(config, all_features, feature_name)


        # THEMES
        if config.has_key('themes_directory'):
            config['themes_directory'] = config['themes_directory']
        else:
            config['themes_directory'] = app_path + os.sep + 'themes'

        themes = {}
        for entry_point in pkg_resources.iter_entry_points('maxe.themes'):
            themes[entry_point.name] = entry_point.load()()  # laod and execute right a way

        theme_config = load_theme({'dir': config['themes_directory'], 'themes': themes}, config['theme'])

        config['cms_templates'] = theme_config['cms_templates']
        config['loaded_themes'] = theme_config['loaded_themes']
        config['theme_asset_paths'] = theme_config['asset_dirs']
        config['theme_template_paths'] = theme_config['template_dirs']

        return config

    except ValueError as e:
        sys.exit("Can not parse config.yaml: " + e.message)



