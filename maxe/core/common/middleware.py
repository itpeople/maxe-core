import logging
import re
from django.utils import translation
from maxe.core.common.signals import on_request
from maxe.core.settings import MAIN_LANGUAGE, ADMIN_LANGUAGE

__author__ = 'alex'

from cms.appresolver import applications_page_check

logger = logging.getLogger('maxe')

class LazyPage(object):
    def __get__(self, request, obj_type=None):
        from cms.utils.page_resolver import get_page_from_request
        if not hasattr(request, '_current_page_cache'):
            request._current_page_cache = get_page_from_request(request)
            if not request._current_page_cache:
                # if this is in a apphook
                # find the page the apphook is attached to
                request._current_page_cache = applications_page_check(request)
        return request._current_page_cache


class CurrentPageMiddleware(object):
    def process_request(self, request):
        request.__class__.current_page = LazyPage()
        return None


class LocaleRewriteMiddleware(object):
    """
    settings should contain MAIN_LANGUAGE definition.
    """

    def process_response(self, request, response):
        if request.path[0:17] == '/admin/auth/user/':
            response.set_cookie('ad_lang', '1', 60 * 60 * 24 * 365)
        return response

    def process_request(self, request):
        if request.path[0:7] == '/admin/':
            lang = ADMIN_LANGUAGE
        else:
            match = re.match('^/([a-z]{2})/', request.path)
            if match:
                lang = match.group(1)
            else:
                lang = MAIN_LANGUAGE

        translation.activate(lang)
        request.LANGUAGE_CODE = translation.get_language()

        return None
