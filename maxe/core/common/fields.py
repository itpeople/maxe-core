from django_ace.widgets import AceWidget
import six
import yaml
from django.db import models
from django.core.serializers.pyyaml import DjangoSafeDumper

class YamlFancyField(models.TextField):

    def formfield(self, **kwargs):
        kwargs["widget"] = AceWidget(mode='yaml')
        return super(YamlFancyField, self).formfield(**kwargs)

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^maxe\.core\.common\.fields\.YamlFancyField"])
except ImportError:
    pass