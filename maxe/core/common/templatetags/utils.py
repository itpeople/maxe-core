from BeautifulSoup import BeautifulSoup
from django.utils.encoding import smart_str
from django.utils.safestring import mark_safe
import re
from maxe.core.content.models import SeoUrl
#from maxe.core.common.templatetags.model_trans import localize
import locale
from maxe.core.settings import APP_SETTINGS


__author__ = 'Alex'

from django import template
register = template.Library()


VALID_TAGS = ['strong', 'em', 'p', 'ul', 'li', 'br', 'img']

from cms.models import Page
from cms.templatetags.cms_tags import PageUrl



class PageUrlOr(PageUrl):
    name = 'page'

    def get_context(self, context, page_lookup, lang, site):
        try:
            context = super(PageUrlOr, self).get_context(context, page_lookup, lang, site)
        except Page.DoesNotExist:
            return {'content': '#'}
        return context

register.tag(PageUrlOr)

@register.filter()
def smartstr(val):
    return smart_str(val)

@register.filter()
def is_category_active(node, current):
    return node.id == current.id or (node.parent and node.parent.id == current.id or (node.parent.parent and node.parent.parent.id == current.id))

@register.filter()
def nospaces(value):
    print(value)
    value = re.sub('/\s+/m', ' ', value)
    print value
    return value

@register.filter()
def money(value, show_zeros=False):

    if not value:
        value = 0
    if not show_zeros:
        return mark_safe(str(int(round(float(value)))) + '&nbsp;' + APP_SETTINGS['currency'])
    return mark_safe(str(locale.currency(float(value), grouping=True)).replace(' ', '&nbsp;'))

@register.filter
def sanitize(value):

    soup = BeautifulSoup(str(value))

    for tag in soup.findAll(True):
        if tag.name not in VALID_TAGS:
            tag.hidden = True

    return soup.renderContents()

@register.filter
def get(d, key):
    return d.get(key)

@register.simple_tag()
def lookup_seo_url(request):

    url = seo_object_lookup(request)

    return url.title + (' - ' if len(url.title) > 0 else '') + 'Inter 24'


def seo_object_lookup(request):

    try:
        url = SeoUrl.objects.get(url=request.path)
    except Exception:
        url = SeoUrl()
        url.title = ""
        url.description = ""
	if hasattr(request, "path"):
	        url.url = request.path
        	url.save()
    return url


@register.simple_tag()
def lookup_seo_description(request):

    url = seo_object_lookup(request)
    return url.description

