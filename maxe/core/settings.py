import platform
import cloud
import locale
import os
from django.core import urlresolvers
import sys
from maxe.core.common.config import load_config

import djcelery
djcelery.setup_loader()

this_folder = os.path.abspath(os.path.dirname(__file__))
cur_path = os.environ.get("MAXE_APP_PATH")
if not cur_path:
    if os.path.exists('app'):
        cur_path = 'app'
    else:
        cur_path = '.'

is_windows = platform.system() == 'Windows'

MAXE_APP_PATH = cur_path

config = load_config(cur_path + '/config.yaml', cur_path)

USE_CMS = True
USE_CMS_ROOT = True

# Django settings for maxe.core project.

DEBUG = config['debug']
TEMPLATE_DEBUG = DEBUG

THEME_NAME = config['theme']

INTERNAL_IPS = ('127.0.0.1',)

ALLOWED_HOSTS = [config['domain'], '127.0.0.1']

ADMINS = (
    ('Alex Rudakov', 'ribozz@gmail.com'),
)

MANAGERS = ADMINS

if 'test' in sys.argv:
    DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3'}}
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': config['dbname'], # Or path to database file if using sqlite3.
            'USER': config['user'], # Not used with sqlite3.
            'PASSWORD': config['password'], # Not used with sqlite3.
            'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '', # Set to empty string for default. Not used with sqlite3.
        }
    }

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Tallinn'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = config['main_lang']

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

LOCALE_PATHS = (
    cur_path + os.sep + 'locale',
)


# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = cur_path + os.sep + 'data' + os.sep + 'media'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = cur_path + os.sep + 'data' + os.sep + 'assets'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/assets/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/assets/admin/'


# Additional locations of static files
STATICFILES_DIRS = tuple(config['theme_asset_paths'])


# Additional locations of static files
TEMPLATE_DIRS = tuple(config['theme_template_paths'])




# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '&!n+&t99!0s1+6+8d-bkhl1=a0&p3lsst^&*k5xhueb+kvt&lj'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    'maxe.core.common.middleware.LocaleRewriteMiddleware',


    #'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',

    'django.contrib.messages.middleware.MessageMiddleware',
    #    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware'
    #        'cms.middleware.multilingual.MultilingualURLMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    #        'cms.middleware.toolbar.ToolbarMiddleware',

)
if not is_windows:
    MIDDLEWARE_CLASSES += ('maxe.core.geo.middleware.FilterByIpMiddleware',)

# if DEBUG:
#     MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

ROOT_URLCONF = 'maxe.core.urls'

CMS_PERMISSION = False
INSTALLED_APPS = (
    #    'tracking',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Uncomment the next line to enable the admin:




    # Uncomment the next line to enable admin documentation:
    'django.contrib.admindocs',

    'django_extensions',
    'hvad',

    'south',
    'maxe.core.smart_selects',

    'django_ace',

    #    'registration',

    'rosetta',

    #'bootstrap_toolkit',
    # "maxe.core.admintools_bootstrap",
    # "admin_tools",
    # "admin_tools.theming",
    # "admin_tools.menu",
    # "admin_tools.dashboard",

    # 'django_admin_bootstrapped',

    'suit',
    'suit_redactor',
    'django.contrib.admin',

    'bootstrapform',

    'mptt',


)

if 'search_enabled' in config and config['search_enabled']:
    INSTALLED_APPS += ('haystack',)

if 'app' in config:
    INSTALLED_APPS += (str(config['app']), )

for feature in config['applications']:
    INSTALLED_APPS += (str(feature), )

INSTALLED_FEATURES = config['features']
INSTALLED_FEATURE_URLS = config['feature_urls']

BLOCKED_COUNTRIES = config['blocked_countries'] if 'blocked_countries' in config else None

CRISPY_TEMPLATE_PACK = 'bootstrap3'

INSTALLED_APPS += (
    'cms',
    'menus',
    'sekizai',

    #'django_jenkins',

    'raven.contrib.django.raven_compat',

    # 'filer',
    'filer',
    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_image',
    'cmsplugin_filer_teaser',
    'cmsplugin_filer_video',

    'easy_thumbnails',

    # 'cms.plugins.file',
    'cms.plugins.flash',
    'cms.plugins.googlemap',
    'cms.plugins.link',
    # 'cms.plugins.picture',
    # 'cms.plugins.teaser',
    'cms.plugins.text',
    # 'cms.plugins.video',
    'cms.plugins.twitter'
)

if 'sentrydsn' in config:
    RAVEN_CONFIG = {
        'dsn': config['sentrydsn'],
    }

JENKINS_TASKS = (
    'django_jenkins.tasks.run_pylint',
    #    'django_jenkins.tasks.with_coverage',
    #    'django_jenkins.tasks.django_tests',
)

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
if config.has_key('auth_backends'):
    for auth in config['auth_backends']:
        AUTHENTICATION_BACKENDS += (str(auth), )


#PAYPAL_TEST = True           # Testing mode on
#PAYPAL_WPP_USER = "aleksa_1346345440_biz_api1.itpeople.ee"      # Get from PayPal
#PAYPAL_WPP_PASSWORD = "1346345466"
#PAYPAL_WPP_SIGNATURE = "ALWfxW97FBy6mUWzolQNJVCjjYTCAvdL6eRddDdQwLmwSSY3l9WZvUrb"

#PAYPAL_TEST = False           # Testing mode on
#PAYPAL_WPP_USER = "dennis_api1.saunamaailm.ee"      # Get from PayPal
#PAYPAL_WPP_PASSWORD = "8LKVQ5YHWU2LC8VA"
#PAYPAL_WPP_SIGNATURE = "A5D7Jlrg2Go65Y-UK6wNdQjSt274ApQ4yQdcIAaLCbFH7LJBlpFA6wdw"

PAYPAL_TEST = None
PAYPAL_WPP_USER = None
PAYPAL_WPP_PASSWORD = None
PAYPAL_WPP_SIGNATURE = None

APPEND_SLASH = False

DEBUG_TOOLBAR_PANELS = (
    #    'debug_toolbar.panels.version.VersionDebugPanel',
    #    'debug_toolbar.panels.timer.TimerDebugPanel',
    #    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    #    'debug_toolbar.panels.headers.HeaderDebugPanel',
    #    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    #    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)


# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        #'mail_admins': {
        #    'level': 'ERROR',
        #    'class': 'django.utils.log.AdminEmailHandler'
        #},
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': cur_path + '/logs/debug.log',
        }
    },
    'loggers': {
        'maxe': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        #'django.request': {
        #    'handlers': ['mail_admins'],
        #    'level': 'ERROR',
        #    'propagate': True,
        #},
        'django': {
            'handlers': ['console', 'file'],
            'level': 'ERROR',
            'propagate': True,
        },
        'keyedcache': {
            'handlers': ['console', 'file'],
            'level': 'ERROR',
            'propagate': True,
        }
    }
}

#import logging


TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.request",
    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
)

# TEMPLATE_CONTEXT_PROCESSORS = (
#     # default template context processors
#     'django.contrib.auth.context_processors.auth',
#     'django.core.context_processors.debug',
#     'django.core.context_processors.i18n',
#     'django.core.context_processors.media',
#     'django.core.context_processors.static',
#     'django.contrib.messages.context_processors.messages',
#
#     # required by django-admin-tools
#     'django.core.context_processors.request',

# )

for processor in config['contexts']:
    TEMPLATE_CONTEXT_PROCESSORS += (str(processor),)

CMS_TEMPLATES = tuple([(path, name) for name, path in config["cms_templates"].items()])
CMS_TEMPLATES = tuple([(path, name) for name, path in config["cms_templates"].items()])

ADMIN_TOOLS_MENU = 'maxe.core.menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'maxe.core.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'maxe.core.dashboard.CustomAppIndexDashboard'

MAIN_LANGUAGE = config['main_lang']
ADMIN_LANGUAGE = config['admin_lang']


LANGUAGES = ()
for lang, lang_name in config['langs'].iteritems():
    LANGUAGES += ((str(lang), lang_name,),)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': '%s_default' % config['user'],
    },
}
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

SOUTH_AUTO_FREEZE_APP = True

THUMBNAIL_KEY_PREFIX = 'thumb2'
THUMBNAIL_PREFIX = 'img-cache2/'
THUMBNAIL_ORIENTATION = False

PAYPAL_RECEIVER_EMAIL = "ribozz@gmail.com"

if 'locale' in config and not is_windows:
    locale.setlocale(locale.LC_ALL, str(config['locale']))
if is_windows:
    locale.setlocale(locale.LC_ALL, 'us')

ACCOUNT_ACTIVATION_DAYS = 30
LOGIN_REDIRECT_URL = '/profile/'

AUTH_PROFILE_MODULE = 'profile.UserProfile'

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_ERROR_URL = '/login-error/'

SHOP_COUNT_PRODUCTS = False
SHOP_DELIVERY_PER_PRODUCT = True
SHOP_PRODUCTS_HIDE_SOLD = False

ROSETTA_WSGI_AUTO_RELOAD = True
ROSETTA_EXCLUDED_APPLICATIONS = ("maxe.ecommerce.profile",)
ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
BING_APP_ID = 'i6uS5VO3OminlgcBr2xBD5A76CZ3DRIRjRT87+IFnNI='
ROSETTA_MESSAGES_PER_PAGE = 20
ROSETTA_STORAGE_CLASS = 'rosetta.storage.SessionRosettaStorage'

USE_FALLBACK_TRANSLATION = False if not 'fallback_i18n' in config else config['fallback_i18n']

THUMBNAIL_FORMAT = 'PNG'

PASS_SALT = 'AKIAIANPQCZBPOJYRMGA-DMHusITS89Yy1a/ZVlkyUvoyu71aRRcZb2DxMrk7'
PASS_SALT2 = 'AKIAIAfkasdjhfafNPQCZBPOJYfelwk;lRMGA-DMHusITS89Yy1a/ZVlkyUvoyu71aRRcZb2DxMrkffew7'

DEPLOYMENT_DOMAIN_NAME = config['domain']

EMAIL_HOST = 'email-smtp.us-east-1.amazonaws.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'AKIAJOVVYA5MNG4QB37Q'
EMAIL_HOST_PASSWORD = 'AugXsT383t0vWfyrFGkQKu0U9qR/IeV1B+IhvSipseWk'
EMAIL_USE_TLS = True
EMAIL_FROM = 'info@%s' % DEPLOYMENT_DOMAIN_NAME

# picloud key
cloud.setkey('5350', 'be0650e76d662c5f7dc0daf0733f9a23c41260a9')

ADMINTOOLS_BOOTSTRAP_SITE_LINK = '/'


def custom_show_toolbar(request):
    return True  # Always show toolbar, for example purposes only.


DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
    'HIDE_DJANGO_SQL': False,
    'TAG': 'body',
    'ENABLE_STACKTRACES': True,
}

# prevent monkeypatching
urlresolvers.reverse.cms_monkeypatched = True
#django.core.urlresolvers.reverse = django.core.urlresolvers.old_reverse

KEY_PREFIX = config['dbname'] + '_'
CMS_CACHE_PREFIX = config['dbname'] + '_CMS_'

if not 'disable_memcahce' in config and not is_windows:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
            'LOCATION': '127.0.0.1:11211',
        }
    }

if 'suit_config' in config:
    SUIT_CONFIG = config['suit_config']
else:
    SUIT_CONFIG = {
        'ADMIN_NAME': config['site_name'],

        'MENU_OPEN_FIRST_CHILD': True,

        'MENU_ICONS': {
            'cms': 'icon-picture',
            # 'cart': 'icon-shopping-cart',
            # 'contact_form': 'icon-envelope',
            # 'content': 'icon-bullhorn',
            'auth': 'icon-lock',
        }

        # 'MENU': (
        #
        #      # Keep original label and models
        #     'sites',
        #
        #     # Rename app and set icon
        #     {'app': 'auth', 'label': 'Authorization', 'icon':'icon-lock'},
        #
        #     # Reorder app models
        #     {'app': 'auth', 'models': ('user', 'group')},
        #
        #     # Custom app, with models
        #     {'label': 'Settings', 'icon':'icon-cog', 'models': ('auth.user', 'auth.group')},
        #
        #     # Cross-linked models with custom name; Hide default icon
        #     {'label': 'Custom', 'icon':None, 'models': (
        #         'auth.group',
        #         {'model': 'auth.user', 'label': 'Staff'}
        #     )},
        #
        #     # Custom app, no models (child links)
        #     {'label': 'Users', 'url': 'auth.user', 'icon':'icon-user'},
        #
        #     # Separator
        #     '-',
        #
        #     # Custom app and model with permissions
        #     {'label': 'Secure', 'permissions': 'auth.add_user', 'models': [
        #         {'label': 'custom-child', 'permissions': ('auth.add_user', 'auth.add_group')}
        #     ]},
        # )
    }

APP_SETTINGS = config['APP_SETTINGS'] if 'APP_SETTINGS' in config else {}

CELERYBEAT_SCHEDULE = {}

if 'tasks' in config:
    for key, val in config['tasks']:  # tuple of 2 items
        CELERYBEAT_SCHEDULE[key] = val

CELERY_TIMEZONE = 'UTC'

SOUTH_TESTS_MIGRATE = False

