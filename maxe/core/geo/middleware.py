"""

"""
import os
from django.contrib.gis.geoip import GeoIP
from django.http import HttpResponse
from maxe.core.settings import BLOCKED_COUNTRIES


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class FilterByIpMiddleware(object):

    api = None

    def __init__(self):

        self.api = GeoIP(path=os.path.dirname(__file__))
        super(FilterByIpMiddleware, self).__init__()

    def process_request(self, request):
        if 'ad_lang' in request.COOKIES:
            return None

        if request.user.is_authenticated():
            return None

        if not BLOCKED_COUNTRIES:
            return None

        if request.path[0:7] == '/admin/':
            return None
        else:
            ip = get_client_ip(request)

            code = self.api.country_code(ip)
            if not code in BLOCKED_COUNTRIES:
                return None
            else:
                return HttpResponse("Website is blocked in your region.")