import os


def core_theme():
    return os.path.dirname(__file__) + '/themes/core'