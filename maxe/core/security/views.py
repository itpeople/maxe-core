import json
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse

__author__ = 'Alex'



def check(request, type):

    if request.user.is_anonymous():
        raise PermissionDenied()

    if type == 'admin' and not request.user.is_superuser:
        raise PermissionDenied()

    return HttpResponse(json.dumps({'ok':'true'}), mimetype='application/json')
