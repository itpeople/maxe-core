from maxe.core.common.admin import default_overrides
from maxe.core.common.translate import  TranslatableAdmin, InlineTranslations
from maxe.core.common.i18n import I18nLabel, message_wrapper
from django.contrib import admin
from maxe.core.content.models import BannerImage, Banner, SeoUrl, Testimonial, TestimonialTranslation


def fix_translations(modeladmin, request, queryset):
    for obj in queryset:
        modeladmin.fix_translations(obj)
fix_translations.short_description = "Fix empty translations"

class SeoUrlAdmin(TranslatableAdmin, admin.ModelAdmin):
    list_display = ( 'url','title',)

    class Media:
        js = (
#            "js/admin/tiny_mce/tiny_mce.js",
#            "js/admin/tinymce_init.js",
            )


class InlineBannerImage(admin.StackedInline):
    model = BannerImage
    extra = 3
    classes = ['collapse']

class BannerAdmin(admin.ModelAdmin):
    inlines = [InlineBannerImage]
    list_display = ('name',)




class InlineTestimonialTranslation(InlineTranslations):
    model = TestimonialTranslation

class TestimonialAdmin(TranslatableAdmin, admin.ModelAdmin):

    #formfield_overrides = default_overrides
    inlines = [InlineTestimonialTranslation]
    init_translations = ['title', 'position', 'text']
    translation_class = TestimonialTranslation
    fields = ('title', 'position', 'avatar', 'text')

    list_display = ('title', 'position', 'text')
    actions = [fix_translations]

admin.site.register(Testimonial, TestimonialAdmin)

admin.site.register(SeoUrl, SeoUrlAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register = I18nLabel(admin.site.register).register()
admin.site.app_index = I18nLabel(admin.site.app_index).index()

admin.ModelAdmin.message_user = message_wrapper(admin.ModelAdmin.message_user)