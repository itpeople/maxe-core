from cms.models.fields import PageField
from django.db import models
from django.db.models.fields import DateTimeField

from django.utils.translation import ugettext_lazy as _
from filer.fields.image import FilerImageField
from maxe.core.common.translate import TranslatableModelMixin

class SeoUrl(TranslatableModelMixin, models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    url = models.CharField(max_length=100)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Seo url')
        verbose_name_plural = _('Seo urls')

class Banner(TranslatableModelMixin, models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name + ', id: ' + str(self.id)

    class Meta:
        verbose_name = _('Banner')
        verbose_name_plural = _('Banners')

class BannerImage(models.Model):
    #    title = models.CharField(max_length=100, blank=True)
    image = FilerImageField(null=True, blank=True)
    banner = models.ForeignKey(Banner)
    title = models.CharField(max_length=100, blank=True, default='', null=False)
    page = PageField(blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)

    def get_url(self):
        if self.url:
            return self.url
        else:
            return '#'

    class Meta:
        verbose_name = _('Slide')
        verbose_name_plural = _('Slides')


class TestimonialTranslation(models.Model):
    lang = models.CharField(max_length=2, editable=False)

    related = models.ForeignKey('Testimonial', null=True, related_name='translations')

    title = models.CharField(max_length=100, null=True, blank=True)
    position = models.CharField(max_length=100, null=True, blank=True)
    text = models.TextField(null=True, blank=True)


class Testimonial(TranslatableModelMixin, models.Model):
    """
    Mentions about site
    """
    avatar = FilerImageField(null=True, blank=True)
    title = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    text = models.TextField()

    class Meta:
        verbose_name = _(u'Testimonial')
        verbose_name_plural = _(u'Testimonials')

    @staticmethod
    def get_translation_class():
        return TestimonialTranslation

#
# class Slider(CMSPlugin):
#     name = models.CharField(max_length=255)
#
#     def __unicode__(self):
#         return self.name + ', id: ' + str(self.id)
#
#     def copy_relations(self, oldinstance):
#         for associated_item in oldinstance.images.all():
#             # instance.pk = None; instance.pk.save() is the slightly odd but
#             # standard Django way of copying a saved model instance
#             associated_item.pk = None
#             associated_item.plugin = self
#             associated_item.save()
#
#     class Meta:
#         verbose_name = _('Slider')
#         verbose_name_plural = _('Sliders')
#
#
# class SliderImage(models.Model):
#     image = FilerImageField(null=True, blank=True)
#     slider = models.ForeignKey(Slider, related_name='images')
#     url = models.CharField(max_length=100, blank=True, null=True)
#
#     def get_url(self):
#         if self.url:
#             return self.url
#         else:
#             return '#'
#
#     class Meta:
#         verbose_name = _('Slide')
#         verbose_name_plural = _('Slides')