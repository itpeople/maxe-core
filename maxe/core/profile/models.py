from django.contrib.auth.signals import user_logged_in
from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    firstname = models.CharField(max_length=255, verbose_name=_('firstname'))
    lastname = models.CharField(max_length=255, verbose_name=_('lastname'))


def create_user_profile(user, **kwargs):
    try:
        user.get_profile()
    except UserProfile.DoesNotExist:
        UserProfile.objects.create(user=user)

user_logged_in.connect(create_user_profile, sender=User)

