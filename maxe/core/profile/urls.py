from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
   url(r'^profile/$', 'maxe.core.profile.views.index', name='profile'),
)
