import logging
from django.core.signals import request_started
from django.dispatch import receiver
from django.test.signals import setting_changed
from django.conf import settings

__author__ = 'Alex'

# import config
#
# logger = logging.getLogger('maxe')
#
# logger.info('Start loading facebook-settings')
#
#
# @receiver(configuration_value_changed)
# def on_change(sender, **kwargs):
#     if sender.key == 'FACEBOOK_APP_ID':
#         settings.FACEBOOK_APP_ID = kwargs['new_value']
#     if sender.key == 'FACEBOOK_API_SECRET':
#         settings.FACEBOOK_API_SECRET = kwargs['new_value']
# #
# @receiver(request_started)
# def on_load(sender, **kwargs):
#
#     if not hasattr(settings, 'FACEBOOK_APP_ID'):
#         settings.FACEBOOK_APP_ID     = config_value('AUTH_FACEBOOK', 'FACEBOOK_APP_ID')
#         settings.FACEBOOK_API_SECRET = config_value('AUTH_FACEBOOK', 'FACEBOOK_API_SECRET')
#         settings.FACEBOOK_EXTENDED_PERMISSIONS = ['email',]
#         settings.SOCIAL_AUTH_PIPELINE = ('social_auth.backends.pipeline.social.social_auth_user',
#                                          'social_auth.backends.pipeline.associate.associate_by_email',
#                                          'social_auth.backends.pipeline.user.get_username',
#                                          'social_auth.backends.pipeline.user.create_user',
#                                          'social_auth.backends.pipeline.social.associate_user',
#                                          'social_auth.backends.pipeline.social.load_extra_data',
#                                          'social_auth.backends.pipeline.user.update_user_details',)
#
#         logger.info('Facebook api id "%s" and other props are injected into settings' % settings.FACEBOOK_APP_ID)
