import cms
from cms.views import details
from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib.admin.sites import NotRegistered
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView
from robots.models import Rule, Url
from common.forms import RegistrationForm
from common.i18n import localize_url as _
from maxe.core.security.views import check

import maxe.core.settings as settings

from django.contrib.auth import views as auth_views
from maxe.core.settings import EMAIL_FROM
# from maxe.ecommerce.shops.views import register


admin.autodiscover()

try:
    admin.site.unregister(Site)
except NotRegistered:
    pass

urlpatterns = patterns('',

    url(r'^robots.txt$', include('robots.urls')),

    url(r'^chaining/', include('maxe.core.smart_selects.urls')),


#    # registration


    # url(_(r'^register/$'),
    #     register,
    #     name='registration_register'),

    url(_(r'^register/closed/$'), TemplateView.as_view(template_name='registration/registration_closed.html'),
        name='registration_disallowed'),

    url(_(r'^login/$'),
        auth_views.login,
            {'template_name': 'registration/login.html'},
        name='auth_login'),
    url(_(r'^logout/$'),
        auth_views.logout,
            {'template_name': 'registration/logout.html'},
        name='auth_logout'),
    url(_(r'^password/change/$'),
        auth_views.password_change,
        name='auth_password_change'),
    url(_(r'^password/change/done/$'),
        auth_views.password_change_done,
        name='auth_password_change_done'),
    url(_(r'^password/reset/$'),
        auth_views.password_reset,
        {'from_email': EMAIL_FROM},
        name='auth_password_reset',),
    url(_(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$'),
        auth_views.password_reset_confirm,
        name='auth_password_reset_confirm'),
    url(_(r'^password/reset/complete/$'),
        auth_views.password_reset_complete,
        name='auth_password_reset_complete'),
    url(_(r'^password/reset/done/$'),
        auth_views.password_reset_done,
        name='auth_password_reset_done'),


    url(r'^auth/check/(?P<type>[a-z]+)$', check, name='check_admin'),

    url(r'^i18n/', include('django.conf.urls.i18n')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/langs/', include('rosetta.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_utils/cache_clear', 'maxe.core.common.views.clear_cache', name='clear_cache'),



    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^assets/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),

#    url(r'^', include('maxe.ecommerce.shops.urls'))
)

for feature in settings.INSTALLED_FEATURE_URLS:
    urlpatterns += patterns('',
        url(r'^', include(str(feature)))
    )

urlpatterns += patterns('',
    url(_(r'^$'), details, {'slug':''}, name='pages-root'),
    url(_(r'^(?P<slug>[0-9A-Za-z-_.//]+)$'), details, name='pages-details-by-slug'),
)
urlpatterns += patterns('',
    url(_(r'^$'), cache_page(60 * 24)(details), {'slug':''}, name='pages-root'),
    url(_(r'^(?P<slug>[0-9A-Za-z-_.//]+)$'), cache_page(60 * 24)(details), name='pages-details-by-slug'),
)
# urlpatterns += patterns('',
# #     url(r'^admin/', include(admin.site.urls)),
#     url(r'^', include('cms.urls')),
# )