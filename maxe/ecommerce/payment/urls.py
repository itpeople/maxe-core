from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:

urlpatterns = patterns('',

    url(r'^payment/dibs/', include('maxe.ecommerce.payment.dibs.urls')),
    url(r'^payment/paypal/', include('maxe.ecommerce.payment.paypal.urls')),
    url(r'^payment/pangalink/', include('maxe.ecommerce.payment.pangalink.urls')),
    url(r'^payment/paytrail/', include('maxe.ecommerce.payment.paytrail.urls')),
    url(r'^payment/kk/', include('maxe.ecommerce.payment.kardikeskus.urls')),
    url(r'^payment/voucher/', include('maxe.ecommerce.payment.voucher.urls')),
)
