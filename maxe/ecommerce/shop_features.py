
def web_shop(config):
    return {
        "icon": "",
        "title": "Awesomebit website",
        "description": "",

        "require": ["cms",],

        "config": {
            "applications": [
                "maxe.ecommerce.profile",
                "maxe.ecommerce.shopping_cart",
                "maxe.ecommerce.payment",
                "maxe.ecommerce.shops",
                "maxe.ecommerce.orders"
            ],
            "feature_urls": [
                "maxe.ecommerce.shopping_cart.urls",
                "maxe.ecommerce.shops.urls",
                "maxe.ecommerce.orders.urls",
            ],
            "contexts": [
                "maxe.core.common.context.common_context",
                "maxe.ecommerce.shops.context.shop_context",
                "maxe.ecommerce.shopping_cart.context.cart_context",
            ],
        }
    }


def web_shop_light(config):
    return {
        "icon": "",
        "title": "Awesomebit website",
        "description": "",

        "require": ["cms",],

        "config": {
            "applications": [
                "maxe.ecommerce.profile",
                "maxe.ecommerce.shopping_cart",
                "maxe.ecommerce.payment",
                "maxe.ecommerce.shops",
                "maxe.ecommerce.orders"
            ],
            "feature_urls": [
                "maxe.ecommerce.shopping_cart.urls",
                #"maxe.ecommerce.shops.urls",
                "maxe.ecommerce.orders.urls",
            ],
            "contexts": [
                "maxe.core.common.context.common_context",
                "maxe.ecommerce.shops.context.shop_context",
                "maxe.ecommerce.shopping_cart.context.cart_context",
            ],
        }
    }


def shop_extra(config):
    return {
        "icon": "",
        "title": "Awesomebit website",
        "description": "",

        "require": [
            #"web-shop",
        ],

        "config": {
            "applications": [
                'maxe.core.content',
                "maxe.ecommerce.features.contact_form",
                'robots',
                ],
            "feature_urls": [
                ],
            "contexts": [
                ],
            }
    }


def shop_payment(config):
    return {
        "icon": "",
        "title": "Awesomebit website",
        "description": "",

        "require": [
            #"web-shop",
        ],

        "config": {
            "applications": [
                "maxe.ecommerce.payment",
                "paypal.standard",
                "paypal.pro",
                ],
            "feature_urls": [
                "maxe.ecommerce.payment.urls",
                ],
            "contexts": [
                ],
            }
    }
