from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from maxe.core.common.i18n import localize_url as _
from maxe.ecommerce.profile.views import Profile, ProfileAddressList, ProfileAddressNew, ProfileOrderList, ProfileOrderDetails, ProfileAddressEdit, ProfileAddressRemove, ProfileEdit
from maxe.ecommerce.shops.sitemap import ProductCategorySitemap, ProductSitemap
from maxe.ecommerce.shops.views import MainPageListView, ProductView, SearchListView, all_products_xml

sitemaps = {
    'categories': ProductCategorySitemap(),
    'products': ProductSitemap(),
    }

urlpatterns = patterns('',

    url(_(r'^$'), MainPageListView.as_view(), name='main_page'),

    url(r'^sitemap\.xml$', 'maxe.ecommerce.shops.sitemap.sitemap', {'sitemaps': sitemaps}),
    url(r'^afesf322fs32fefwkj32iiwefw/all_products\.xml$', all_products_xml),

    url(_(r'^product/all/$'), SearchListView.as_view(), name='all_products'),
    url(_(r'^product/$'), SearchListView.as_view()),

    url(_(r'^product/(?P<category_id>\d+)$'),
        SearchListView.as_view(),
        name='products_in_category'
    ),

    url(_(r'^product/all/(?P<pk>\d+)$'),
        ProductView.as_view(),
        name='product'
    ),

    url(_(r'^product/(?P<category_id>\d+)/(?P<pk>\d+)$'),
        ProductView.as_view(),
        name='product_with_cat'
    ),

    url(_(r'^product/all/(?P<slug>[^/]+)$'),
        ProductView.as_view(),
        name='product_by_slug'
    ),

    url(_(r'^product/(?P<category_slug>[^/]+)$'),
        SearchListView.as_view(),
        name='products_in_category_by_slug'
    ),

    url(_(r'^product/(?P<category_slug>[^/]+)/(?P<pk>\d+)$'),
        ProductView.as_view(),
        name='product_with_cat_cat_by_slug_prod_by_id'
    ),

    url(_(r'^product/(?P<category_id>\d+)/(?P<slug>[^/]+)$'),
        ProductView.as_view(),
        name='product_with_cat_cat_by_id_prod_by_slug'
    ),

    url(_(r'^product/(?P<category_slug>[^/]+)/(?P<slug>[^/]+)$'),
        ProductView.as_view(),
        name='product_with_cat_both_by_slug'
    ),


    url(_(r'^profile/$'),
        Profile.as_view(),
        name='profile'
    ),

    url(_(r'^profile/edit$'),
        ProfileEdit.as_view(),
        name='profile_edit'
    ),

    url(_(r'^profile/address/$'),
        ProfileAddressList.as_view(),
        name='profile_address'
    ),
    url(_(r'^profile/address/make_default/(?P<id>\d+)$'),
        'maxe.ecommerce.profile.views.make_address_default',
        name='profile_address_make_default'
    ),
    url(_(r'^profile/address/update_address/(?P<id>\d+)$'),
        'maxe.ecommerce.profile.views.update_address',
        name='profile_update_address'
    ),
    url(_(r'^profile/orders/$'),
        ProfileOrderList.as_view(),
        name='profile_orders'
    ),
    url(_(r'^profile/orders/detail/(?P<pk>\d+)$'),
        ProfileOrderDetails.as_view(),
        name='profile_order_details'
    ),

    url(_(r'^profile/address/new$'),
        ProfileAddressNew.as_view(),
        name='profile_address_new'
    ),

    url(_(r'^profile/address/edit/(?P<pk>\d+)$'),
        ProfileAddressEdit.as_view(),
        name='profile_address_edit'
    ),


    url(_(r'^profile/address/remove/(?P<pk>\d+)$'),
        ProfileAddressRemove.as_view(),
        name='profile_address_remove'
    ),

    url(r'^compare/add/(?P<product_id>\d+)$', 'maxe.ecommerce.shops.views.add_to_compare', name='compare_add'),
    url(r'^compare/clear$', 'maxe.ecommerce.shops.views.clear_compare', name='compare_clear'),
    url(r'^compare/render$', 'maxe.ecommerce.shops.views.render_compare', name='compare_render'),

    url(r'^order/reject/(?P<order_id>\d+)$', 'maxe.ecommerce.shops.views.reject_order', name='reject_order'),
    url(r'^order/repeat/(?P<order_id>\d+)$', 'maxe.ecommerce.shops.views.repeat_order', name='repeat_order'),

    url(r'^trans$', 'maxe.ecommerce.shops.views.trans'),
    url(r'^type-ac/(?P<type>\d+)$', 'maxe.ecommerce.shops.views.attr_type'),

    url(_(r'^ajax-auth/login$'), 'maxe.ecommerce.shops.views.ajax_login', name='ajax_login'),
    url(_(r'^ajax-auth/register$'), 'maxe.ecommerce.shops.views.ajax_register', name='ajax_register'),
    url(_(r'^ajax-auth/password_restore$'), 'maxe.ecommerce.shops.views.ajax_password_restore', name='ajax_password_restore'),

)
