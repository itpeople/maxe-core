# encoding: utf-8
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'ProductAttributeValue'
        db.create_table('shops_productattributevalue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('attribute', self.gf('django.db.models.fields.related.ForeignKey')(related_name='productattributevalue_related', null=True, to=orm['shops.ProductAttributeType'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('shops', ['ProductAttributeValue'])

        # Adding model 'ProductAttributeType'
        db.create_table('shops_productattributetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('shops', ['ProductAttributeType'])

        # Adding M2M table for field values on 'ProductAttributeType'
        db.create_table('shops_productattributetype_values', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('productattributetype', models.ForeignKey(orm['shops.productattributetype'], null=False)),
            ('productattributevalue', models.ForeignKey(orm['shops.productattributevalue'], null=False))
        ))
        db.create_unique('shops_productattributetype_values', ['productattributetype_id', 'productattributevalue_id'])

        # Adding model 'ProductAttribute'
        db.create_table('shops_productattribute', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('attribute_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['shops.ProductAttributeType'])),
        ))
        db.send_create_signal('shops', ['ProductAttribute'])

        # Adding M2M table for field values on 'ProductAttribute'
        db.create_table('shops_productattribute_values', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('productattribute', models.ForeignKey(orm['shops.productattribute'], null=False)),
            ('productattributevalue', models.ForeignKey(orm['shops.productattributevalue'], null=False))
        ))
        db.create_unique('shops_productattribute_values', ['productattribute_id', 'productattributevalue_id'])

        # Changing field 'PageTranslation.text'
        db.alter_column('shops_pagetranslation', 'text', self.gf('django.db.models.fields.TextField')(null=True))

        # Adding M2M table for field attributes on 'Product'
        db.create_table('shops_product_attributes', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('product', models.ForeignKey(orm['shops.product'], null=False)),
            ('productattribute', models.ForeignKey(orm['shops.productattribute'], null=False))
        ))
        db.create_unique('shops_product_attributes', ['product_id', 'productattribute_id'])

        # Adding M2M table for field attribute_types on 'ProductCategory'
        db.create_table('shops_productcategory_attribute_types', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('productcategory', models.ForeignKey(orm['shops.productcategory'], null=False)),
            ('productattributetype', models.ForeignKey(orm['shops.productattributetype'], null=False))
        ))
        db.create_unique('shops_productcategory_attribute_types', ['productcategory_id', 'productattributetype_id'])


    def backwards(self, orm):
        
        # Deleting model 'ProductAttributeValue'
        db.delete_table('shops_productattributevalue')

        # Deleting model 'ProductAttributeType'
        db.delete_table('shops_productattributetype')

        # Removing M2M table for field values on 'ProductAttributeType'
        db.delete_table('shops_productattributetype_values')

        # Deleting model 'ProductAttribute'
        db.delete_table('shops_productattribute')

        # Removing M2M table for field values on 'ProductAttribute'
        db.delete_table('shops_productattribute_values')

        # Changing field 'PageTranslation.text'
        db.alter_column('shops_pagetranslation', 'text', self.gf('django.db.models.fields.TextField')(default=''))

        # Removing M2M table for field attributes on 'Product'
        db.delete_table('shops_product_attributes')

        # Removing M2M table for field attribute_types on 'ProductCategory'
        db.delete_table('shops_productcategory_attribute_types')


    models = {
        'shops.page': {
            'Meta': {'object_name': 'Page'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.pagetranslation': {
            'Meta': {'object_name': 'PageTranslation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['shops.Page']"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'shops.product': {
            'Meta': {'object_name': 'Product'},
            'attributes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttribute']", 'symmetrical': 'False'}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductCategory']", 'symmetrical': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'sold': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.productattribute': {
            'Meta': {'object_name': 'ProductAttribute'},
            'attribute_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shops.ProductAttributeType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'values': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttributeValue']", 'symmetrical': 'False'})
        },
        'shops.productattributetype': {
            'Meta': {'object_name': 'ProductAttributeType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'values': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttributeValue']", 'symmetrical': 'False'})
        },
        'shops.productattributevalue': {
            'Meta': {'object_name': 'ProductAttributeValue'},
            'attribute': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'productattributevalue_related'", 'null': 'True', 'to': "orm['shops.ProductAttributeType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.productcategory': {
            'Meta': {'object_name': 'ProductCategory'},
            'allow_filtering': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'attribute_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttributeType']", 'symmetrical': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['shops.ProductCategory']"})
        },
        'shops.productcategorytranslation': {
            'Meta': {'object_name': 'ProductCategoryTranslation'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['shops.ProductCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.productimage': {
            'Meta': {'object_name': 'ProductImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shops.Product']"})
        },
        'shops.producttranslation': {
            'Meta': {'object_name': 'ProductTranslation'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['shops.Product']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['shops']
