# encoding: utf-8
from south.db import db
from south.v2 import SchemaMigration

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'ProductAttributeValue.type'
        db.add_column('shops_productattributevalue', 'type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['shops.ProductAttributeType'], null=True), keep_default=False)

        # Deleting field 'ProductAttribute.attribute_type'
        db.delete_column('shops_productattribute', 'attribute_type_id')

        # Adding field 'ProductAttribute.type'
        db.add_column('shops_productattribute', 'type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['shops.ProductAttributeType'], null=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'ProductAttributeValue.type'
        db.delete_column('shops_productattributevalue', 'type_id')

        # User chose to not deal with backwards NULL issues for 'ProductAttribute.attribute_type'
        raise RuntimeError("Cannot reverse this migration. 'ProductAttribute.attribute_type' and its values cannot be restored.")

        # Deleting field 'ProductAttribute.type'
        db.delete_column('shops_productattribute', 'type_id')


    models = {
        'shops.page': {
            'Meta': {'object_name': 'Page'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.pagetranslation': {
            'Meta': {'object_name': 'PageTranslation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['shops.Page']"}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'shops.product': {
            'Meta': {'object_name': 'Product'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductCategory']", 'symmetrical': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '8', 'decimal_places': '2'}),
            'sold': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.productattribute': {
            'Meta': {'object_name': 'ProductAttribute'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shops.Product']", 'null': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shops.ProductAttributeType']", 'null': 'True'}),
            'values': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttributeValue']", 'symmetrical': 'False'})
        },
        'shops.productattributetype': {
            'Meta': {'object_name': 'ProductAttributeType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'values': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttributeValue']", 'symmetrical': 'False'})
        },
        'shops.productattributevalue': {
            'Meta': {'object_name': 'ProductAttributeValue'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shops.ProductAttributeType']", 'null': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.productcategory': {
            'Meta': {'object_name': 'ProductCategory'},
            'allow_filtering': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'attribute_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['shops.ProductAttributeType']", 'symmetrical': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parent': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['shops.ProductCategory']"})
        },
        'shops.productcategorytranslation': {
            'Meta': {'object_name': 'ProductCategoryTranslation'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['shops.ProductCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'shops.productimage': {
            'Meta': {'object_name': 'ProductImage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['shops.Product']"})
        },
        'shops.producttranslation': {
            'Meta': {'object_name': 'ProductTranslation'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'translations'", 'null': 'True', 'to': "orm['shops.Product']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['shops']
