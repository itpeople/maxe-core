from django.db.models import Count
from maxe.core.content.models import Banner
from maxe.ecommerce.shopping_cart.models import DeliveryMethod
from maxe.ecommerce.shops.models import ProductCategory

def shop_context(request):
    """
    @type context: HttpRequest
    """

    if request.path[0:7] == '/admin/':
        return {}

    context = {
        'categories': ProductCategory.objects.all(),
        'categories_count': dict([(cat.id, cat.product__count) for cat in ProductCategory.objects.filter(product__active=True).annotate(Count('product'))]),
        'banners': dict([(page.name, page) for page in Banner.objects.all()]),
        'delivery_methods': DeliveryMethod.objects.filter(enabled=True)
    }
    return context