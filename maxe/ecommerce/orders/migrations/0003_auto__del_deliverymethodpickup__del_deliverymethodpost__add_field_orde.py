# encoding: utf-8
from south.db import db
from south.v2 import SchemaMigration

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting model 'DeliveryMethodPickUp'
        db.delete_table('orders_deliverymethodpickup')

        # Deleting model 'DeliveryMethodPost'
        db.delete_table('orders_deliverymethodpost')

        # Adding field 'Order.delivery_method'
        db.add_column('orders_order', 'delivery_method', self.gf('django.db.models.fields.CharField')(max_length=40, null=True), keep_default=False)

        # Adding field 'Order.payment_method'
        db.add_column('orders_order', 'payment_method', self.gf('django.db.models.fields.CharField')(max_length=40, null=True), keep_default=False)

        # Adding field 'Order.address'
        db.add_column('orders_order', 'address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orders.Address'], null=True), keep_default=False)

        # Adding field 'Address.recipient'
        db.add_column('orders_address', 'recipient', self.gf('django.db.models.fields.CharField')(max_length=150, null=True), keep_default=False)


    def backwards(self, orm):
        
        # Adding model 'DeliveryMethodPickUp'
        db.create_table('orders_deliverymethodpickup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal('orders', ['DeliveryMethodPickUp'])

        # Adding model 'DeliveryMethodPost'
        db.create_table('orders_deliverymethodpost', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orders.Address'])),
        ))
        db.send_create_signal('orders', ['DeliveryMethodPost'])

        # Deleting field 'Order.delivery_method'
        db.delete_column('orders_order', 'delivery_method')

        # Deleting field 'Order.payment_method'
        db.delete_column('orders_order', 'payment_method')

        # Deleting field 'Order.address'
        db.delete_column('orders_order', 'address_id')

        # Deleting field 'Address.recipient'
        db.delete_column('orders_address', 'recipient')


    models = {
        'cart.cart': {
            'Meta': {'ordering': "('-creation_date',)", 'object_name': 'Cart'},
            'checked_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'orders.address': {
            'Meta': {'object_name': 'Address'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_index': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'recipient': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'street_address': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'orders.order': {
            'Meta': {'object_name': 'Order'},
            'address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orders.Address']", 'null': 'True'}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cart.Cart']"}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'delivery_method': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment_method': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orders.OrderStatus']"})
        },
        'orders.orderstatus': {
            'Meta': {'object_name': 'OrderStatus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['orders']
