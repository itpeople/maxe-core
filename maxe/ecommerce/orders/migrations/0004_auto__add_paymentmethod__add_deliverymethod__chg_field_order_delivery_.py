# encoding: utf-8
from south.db import db
from south.v2 import SchemaMigration

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'PaymentMethod'
        db.create_table('orders_paymentmethod', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('discount', self.gf('django.db.models.fields.FloatField')(default=0.0)),
        ))
        db.send_create_signal('orders', ['PaymentMethod'])

        # Adding model 'DeliveryMethod'
        db.create_table('orders_deliverymethod', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('delivery_price', self.gf('django.db.models.fields.DecimalField')(max_digits=18, decimal_places=2)),
        ))
        db.send_create_signal('orders', ['DeliveryMethod'])

        # Renaming column for 'Order.delivery_method' to match new field type.
        db.rename_column('orders_order', 'delivery_method', 'delivery_method_id')
        # Changing field 'Order.delivery_method'
        db.alter_column('orders_order', 'delivery_method_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orders.DeliveryMethod'], null=True))

        # Adding index on 'Order', fields ['delivery_method']
        db.create_index('orders_order', ['delivery_method_id'])

        # Renaming column for 'Order.payment_method' to match new field type.
        db.rename_column('orders_order', 'payment_method', 'payment_method_id')
        # Changing field 'Order.payment_method'
        db.alter_column('orders_order', 'payment_method_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orders.PaymentMethod'], null=True))

        # Adding index on 'Order', fields ['payment_method']
        db.create_index('orders_order', ['payment_method_id'])


    def backwards(self, orm):
        
        # Removing index on 'Order', fields ['payment_method']
        db.delete_index('orders_order', ['payment_method_id'])

        # Removing index on 'Order', fields ['delivery_method']
        db.delete_index('orders_order', ['delivery_method_id'])

        # Deleting model 'PaymentMethod'
        db.delete_table('orders_paymentmethod')

        # Deleting model 'DeliveryMethod'
        db.delete_table('orders_deliverymethod')

        # Renaming column for 'Order.delivery_method' to match new field type.
        db.rename_column('orders_order', 'delivery_method_id', 'delivery_method')
        # Changing field 'Order.delivery_method'
        db.alter_column('orders_order', 'delivery_method', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Renaming column for 'Order.payment_method' to match new field type.
        db.rename_column('orders_order', 'payment_method_id', 'payment_method')
        # Changing field 'Order.payment_method'
        db.alter_column('orders_order', 'payment_method', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))


    models = {
        'cart.cart': {
            'Meta': {'ordering': "('-creation_date',)", 'object_name': 'Cart'},
            'checked_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'orders.address': {
            'Meta': {'object_name': 'Address'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_index': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'recipient': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'street_address': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        'orders.deliverymethod': {
            'Meta': {'object_name': 'DeliveryMethod'},
            'delivery_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '18', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'orders.order': {
            'Meta': {'object_name': 'Order'},
            'address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orders.Address']", 'null': 'True'}),
            'cart': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cart.Cart']"}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'delivery_method': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orders.DeliveryMethod']", 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment_method': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orders.PaymentMethod']", 'null': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True'}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orders.OrderStatus']"})
        },
        'orders.orderstatus': {
            'Meta': {'object_name': 'OrderStatus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'orders.paymentmethod': {
            'Meta': {'object_name': 'PaymentMethod'},
            'discount': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        }
    }

    complete_apps = ['orders']
