__author__ = 'Alex'

from django.conf.urls.defaults import patterns, url

# Uncomment the next two lines to enable the admin:

urlpatterns = patterns('maxe.ecommerce.features.contact_form.views',

    url(r'^send$', 'form_send', name='contact_form_send'),
)