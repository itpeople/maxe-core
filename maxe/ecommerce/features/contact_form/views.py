import json
from django.http import HttpResponse
from maxe.ecommerce.features.contact_form.models import ContactFormMessage

__author__ = 'Alex'

def form_send(request):
    errors = []
    message = None
    ok = False

    if request.method == 'POST':

        message = ContactFormMessage()
        message.name = request.POST['name']
        message.email = request.POST['email']
        message.invoice_number = request.POST['invoice_number']
        message.text = request.POST['text']

        message.save()
        ok = True

    return HttpResponse(json.dumps({'ok':ok, 'errors': errors, 'message': message}), mimetype='application/json')

