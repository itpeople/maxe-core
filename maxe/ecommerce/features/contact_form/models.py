__author__ = 'Alex'

from django.db import models
from django.db.models.fields import DateTimeField
from django.utils.translation import ugettext_lazy as _

class ContactFormMessage(models.Model):

    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    name = models.CharField(max_length=100, verbose_name=_('Full name'), null=True, blank=True)
    email = models.CharField(max_length=100, verbose_name=_('Email'), null=True, blank=True)
    invoice_number = models.CharField(max_length=100, verbose_name=_('Invoice number'), null=True, blank=True)
    text = models.TextField(blank=True, default='')

    def __unicode__(self):
        return self.type.name
