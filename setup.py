import os
from setuptools import setup, find_packages
# from tests import PyTest

setup(
    name='maxe-core',
    version='0.1.0',
    packages=find_packages(),

    #namespace_packages=['maxe'],

    url='',
    license='Private paid',
    author='Alex Rudakov',
    author_email='ribozz@gmail.com',
    description='Maxe Core system',
    long_description='',
    install_requires=[
        'django-mptt==0.5.2',
        'django==1.5.4',
        'BeautifulSoup',
        'south',
        'django-registration==0.8',
        'django-keyedcache',

        'MySQL-python',
        'django-robots',
        'django-cms==2.4.1',
        'django-hvad',
        'django-rosetta',
        'cloud',
        'django-social-auth',
        'django-extensions',
        # 'pysendfile',
        # 'pylibmc',
        'django-suit',
        'django-filer',
        'cmsplugin-filer',
        'pillow',

        'django-suit-redactor',
        'django-chosen',

        'django-countries',
        'schema==0.2.0',

        'django-celery',
        'pytest',

        'pyyaml',

        'django-theherk-external-urls',
        'django-ace',
        'voluptuous',
        'django-bootstrap-form',
        'raven',
        'M2Crypto==0.21.1'
    ],

    package_data={
            'maxe.core': [
                'themes/**',
            ],
            'maxe.core.geo': [
                'GeoIP.dat',
            ],
        },


    dependency_links=[
        'https://github.com/ribozz/django-haystack/tarball/master#egg=django-haystack',
        'https://pypi.fury.io/TRsw5NWCYoNR6xLoSnyB/ribozz/'
    ],


    extras_require={
        'memcache': ['pylibmc'],
        'deployment': ['gunicorn'],
        'shop': ['django-paypal',
        'pyelasticsearch==0.5',
        'django-haystack']
    },


    entry_points={
        'maxe.features': [
            'cms = maxe.core.features:cms',
            'core_profile = maxe.core.features:profile',
            'debug = maxe.core.features:debug',
            'social_auth = maxe.core.features:social_auth',

            'web-shop = maxe.ecommerce.shop_features:web_shop',
            'web-shop-light = maxe.ecommerce.shop_features:web_shop_light',
            'shop-extra = maxe.ecommerce.shop_features:shop_extra',
            'shop-payment = maxe.ecommerce.shop_features:shop_payment',
        ],
    },

)
